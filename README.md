# README #
moodle-block_course_status

### Moodle Course Status ###
A block to display visibility of a course on a course page.  Allows a user with appropriate permissions to publish / unpublish a course. I.E. Make it visible or not visible.

# General configuration #

 * "Course Status" - Set a title for the block.  Leave empty for no title (default).
 * Published icon - Set the fontawesome icon to use within the button for published courses (default is check-circle)
 * Unpublished icon - Set the fontawesome icon to use within the button for unpublished courses (default is times-circle)

# Per instance configuration  #


# Guidelines for use #

Simply add to a course page in order to use the block. Change settings of the block to get it to display on any course pge.

Version 1.0.5 (2020041301)

### How do I get set up? ###

Installs at <moodleroot>/blocks/course_status

## Settings ##

Site-wide configuration options are available under: 
Site Administration -> Plugins -> Blocks -> Course Status

Per Instance block settings are available by editing block configuration.

### Compatibility ###

- Moodle 3.7, 3.8

### Contribution ###

Developed by:

 * Manoj Solanki (Coventry University)

Co-maintained by:

 * Jeremy Hopkins (Coventry University)
 
 ### Licenses ###

Adaptable is licensed under:
GPL v3 (GNU General Public License) - http://www.gnu.org/licenses
